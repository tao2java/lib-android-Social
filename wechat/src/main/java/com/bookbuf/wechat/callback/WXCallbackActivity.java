package com.bookbuf.wechat.callback;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.bookbuf.social.PlatformConfiguration;
import com.bookbuf.social.PlatformEnum;
import com.bookbuf.social.external.SocialAPI;
import com.bookbuf.social.platforms.WeChatPlatform;
import com.bookbuf.wechat.WXSSOHandler;
import com.tencent.mm.sdk.openapi.BaseReq;
import com.tencent.mm.sdk.openapi.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;

/**
 * Created by robert on 16/6/30.
 */
public class WXCallbackActivity extends Activity implements IWXAPIEventHandler {

	private WXSSOHandler handler;

	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate (savedInstanceState);

		SocialAPI api = SocialAPI.getInstance (this);
		this.handler = (WXSSOHandler) api.getHandler (PlatformEnum.WX_SCENE);
		this.handler.onCreate (this, (WeChatPlatform) PlatformConfiguration.getPlatform (PlatformEnum.WX_SCENE));
		this.handler.getWXApi ().handleIntent (getIntent (), this);
	}

	@Override
	protected void onNewIntent (Intent intent) {
		super.onNewIntent (intent);
		this.setIntent (intent);

		SocialAPI api = SocialAPI.getInstance (this);
		this.handler = (WXSSOHandler) api.getHandler (PlatformEnum.WX_SCENE);
		this.handler.onCreate (this, (WeChatPlatform) PlatformConfiguration.getPlatform (PlatformEnum.WX_SCENE));
		this.handler.getWXApi ().handleIntent (getIntent (), this);

	}

	@Override
	public void onReq (BaseReq req) {
		if (this.handler != null) {
			this.handler.getWXEventHandler ().onReq (req);
		}
		this.finish ();
	}

	@Override
	public void onResp (BaseResp resp) {
		if (this.handler != null && resp != null) {
			this.handler.getWXEventHandler ().onResp (resp);
		}
		this.finish ();
	}
}
