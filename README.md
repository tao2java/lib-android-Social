#lib-android-Social


Android 封装第三方登陆与分享组件

有任何建议或反馈 [请联系: chenjunqi.china@gmail.com](mailto:chenjunqi.china@gmail.com)

欢迎大家加入`android 开源项目群(369194705)`, 有合适的项目大家一起 `fork`;

# 简介
1. 当下仅接入了微信(会话)/微信朋友圈/微信收藏服务---> 详情见(:wechat)
2. 社会化组件中没有定义丰富的媒介类型,包括不限于:图片/视频/音乐/网页/表情等;暂时仅支持纯文本分享,如果需要实现其他媒介分享,
请自行构建对应的行为(包括不限于:构建缩略图/构建URL等)
3. 关于扩展第三方服务(如:QQ/易信/FaceBook等)

> 1. 自行构建新的 `module`(_可参考: wechat_),理论上只需在对应的 `module` 中接入第三方服务的接口,即可正常调用;
> 2. 在枚举类`PlatformEnum`中定义对应` module`的 `handler` , 同时不要忘记更改`PlatformEnum.getSupportPlatforms()`方法;
> 3. 实现接口`IPlatform`,构建对应的平台配置类;
> 4. 在类`PlatformConfiguration`中实现`setXXPlatform()`方法, 供客户端传入`appid`与`appsecret`;

4. 目前还未实现**下图**的效果; _如果你需要实现展示分享平台面板功能. 需要涉及`SnsPlatform`,`ShareAction`类;_

> 主要是目前产品还没有需求接入多SnsPlatform, 所以我就留空了~~ :)

![QQ20160701-0@2x.png](QQ20160701-0@2x.png)

# 使用场景
1. 第三方登陆
2. 第三方分享

## 1.获取SocialAPI

```
SocialAPI service = SocialAPI.getInstance (this);
```

## 2.申请授权
```

service.runOauthApply (this, platform, new ISSOHandler.AuthListener () {
        			@Override
        			public void onComplete (PlatformEnum platform, int action, Map<String, String> map) {
        				Log.d (TAG, "[AuthListener] onComplete: platform = " + platform);
        				Log.d (TAG, "[AuthListener] onComplete: action = " + action);
        				Log.d (TAG, "[AuthListener] onComplete: map = " + map);
        			}

        			@Override
        			public void onError (PlatformEnum platform, int action, Throwable throwable) {
        				Log.d (TAG, "[AuthListener] onError: platform = " + platform);
        				Log.d (TAG, "[AuthListener] onError: action = " + action);
        				Log.d (TAG, "[AuthListener] onError: throwable = " + throwable);
        			}

        			@Override
        			public void onCancel (PlatformEnum platform, int action) {
        				Log.d (TAG, "[AuthListener] onCancel: platform = " + platform);
        				Log.d (TAG, "[AuthListener] onCancel: action = " + action);
        			}
        		});

```

## 3. 删除授权

```

service.runOauthDelete (this, platform, new ISSOHandler.AuthListener () {
        			@Override
        			public void onComplete (PlatformEnum platform, int action, Map<String, String> map) {
        				Log.d (TAG, "[AuthListener] onComplete: platform = " + platform);
        				Log.d (TAG, "[AuthListener] onComplete: action = " + action);
        				Log.d (TAG, "[AuthListener] onComplete: map = " + map);
        			}

        			@Override
        			public void onError (PlatformEnum platform, int action, Throwable throwable) {
        				Log.d (TAG, "[AuthListener] onError: platform = " + platform);
        				Log.d (TAG, "[AuthListener] onError: action = " + action);
        				Log.d (TAG, "[AuthListener] onError: throwable = " + throwable);
        			}

        			@Override
        			public void onCancel (PlatformEnum platform, int action) {
        				Log.d (TAG, "[AuthListener] onCancel: platform = " + platform);
        				Log.d (TAG, "[AuthListener] onCancel: action = " + action);
        			}
        		});

```

## 完成文本分享
```
// 构建分享内容
ShareContent shareContent = new ShareAction.Builder ()
				.setTargetUrl ("[A]www.healthbok.com")
				.setText ("分享内容来自 bookbuf 分享组件.")
				.setTitle ("分享标题来自 bookbuf 分享组件.")
				.build ();
// 设定分享回调
IShareHandler.ShareListener shareListener = new IShareHandler.ShareListener () {
	@Override
	public void onResult (PlatformEnum shareMedia) {
		Log.d (TAG, "[ShareListener] onResult: shareMedia = " + shareMedia);
	}

	@Override
	public void onError (PlatformEnum shareMedia, Throwable throwable) {
		Log.d (TAG, "[ShareListener] onError: shareMedia = " + shareMedia + ", throwable = " + throwable);
	}

	@Override
	public void onCancel (PlatformEnum shareMedia) {
		Log.d (TAG, "[ShareListener] onCancel: shareMedia = " + shareMedia);
	}
};
// 构建分享行为
ShareAction action = new ShareAction (this)
		.setShareContent (shareContent)
		.setShareForm ("[B]www.healthbok.com")
		.setSharePlatform (platform)
		.setShareListener (shareListener);
// 触发分享行为
service.runShare (this, action, action.getShareListener ());

```
# 截图
![device-2016-07-01-132550.png](device-2016-07-01-132550.png)