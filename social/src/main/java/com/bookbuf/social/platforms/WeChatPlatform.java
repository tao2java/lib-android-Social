package com.bookbuf.social.platforms;

import android.text.TextUtils;

import com.bookbuf.social.PlatformEnum;

import org.json.JSONObject;

/**
 * Created by robert on 16/6/29.
 */
public class WeChatPlatform implements IPlatform {

	private final PlatformEnum media;
	public String appId = null;
	public String appSecret = null;

	public WeChatPlatform (PlatformEnum media) {
		this.media = media;
	}

	@Override
	public String[] dependencyPermissions () {
		return new String[]{
				"android.permission.INTERNET",
				"android.permission.ACCESS_NETWORK_STATE",
				"android.permission.ACCESS_WIFI_STATE",
				"android.permission.READ_PHONE_STATE",
				"android.permission.WRITE_EXTERNAL_STORAGE"
		};
	}

	@Override
	public PlatformEnum getName () {
		return this.media;
	}

	@Override
	public void parse (JSONObject var1) {

	}

	@Override
	public boolean isConfigured () {
		return !TextUtils.isEmpty (this.appId) && !TextUtils.isEmpty (this.appSecret);
	}

	@Override
	public boolean isAuthorized () {
		return false;
	}
}
