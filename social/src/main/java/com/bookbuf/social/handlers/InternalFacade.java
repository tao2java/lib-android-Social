package com.bookbuf.social.handlers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.bookbuf.social.PlatformConfiguration;
import com.bookbuf.social.PlatformEnum;
import com.bookbuf.social.handlers.share.action.ShareAction;
import com.bookbuf.social.platforms.IPlatform;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by robert on 16/6/29.
 */
public final class InternalFacade {

	private final static String TAG = "InternalFacade";
	private final Map<PlatformEnum, SSOHandler> instanceMap = new HashMap ();
	private final List<PlatformEnum> configurations;
	private InnerHelper helper;
	private Context context;

	public InternalFacade (Context context) {
		this.configurations = PlatformEnum.getSupportPlatforms ();
		this.helper = new InnerHelper (instanceMap);
		this.context = context;
		this.createHandlerInstances ();
	}

	private void createHandlerInstances () {
		SSOHandler ssoHandler;
		for (PlatformEnum configuration : configurations) {
			PlatformEnum media = configuration;
			String handlerClazz = configuration.getHandlerConfiguration ();

			if (media == PlatformEnum.WX_SCENE_TIMELINE ||
					media == PlatformEnum.WX_SCENE_FAVORITE) {
				ssoHandler = instanceMap.get (PlatformEnum.WX_SCENE);
			} else {
				ssoHandler = obtainHandler (handlerClazz);
			}
			instanceMap.put (media, ssoHandler);
		}

	}

	private SSOHandler obtainHandler (String clazzName) {
		SSOHandler handler = null;
		try {
			Class clazz = Class.forName (clazzName);
			handler = (SSOHandler) clazz.newInstance ();
		} catch (ClassNotFoundException e) {
			e.printStackTrace ();
		} catch (InstantiationException e) {
			e.printStackTrace ();
		} catch (IllegalAccessException e) {
			e.printStackTrace ();
		}
		return handler;
	}

	public SSOHandler obtainHandler (PlatformEnum shareMedia) {
		final Context context = this.context;
		SSOHandler handler = instanceMap.get (shareMedia);
		if (handler != null) {
			IPlatform platform = PlatformConfiguration.getPlatform (shareMedia);
			handler.onCreate (context, platform);
		}
		return handler;
	}

	public void onActivityResult (int requestCode, int resultCode, Intent intent) {
		SSOHandler handler = obtainHandler (requestCode);
		if (handler != null) {
			handler.onActivityResult (requestCode, resultCode, intent);
		}
	}

	private SSOHandler obtainHandler (int requestCode) {
		// TODO: 16/6/29 对比 requestCode 获得SSOHandler
		throw new IllegalArgumentException ("please implement this.");
	}

	public void add (PlatformEnum media, SSOHandler handler) {
		if (media != null && handler != null) {
			this.instanceMap.put (media, handler);
		}
	}

	private ISSOHandler.AuthListener obtainListener () {
		return new ISSOHandler.AuthListener () {
			@Override
			public void onComplete (PlatformEnum platform, int action, Map<String, String> map) {
				Log.d (TAG, "onComplete: platform = " + platform + ",action = " + action + ",map = " + map);
			}

			@Override
			public void onError (PlatformEnum platform, int action, Throwable throwable) {
				Log.d (TAG, "onError: platform = " + platform + ",action = " + action + ",throwable = " + throwable);
			}

			@Override
			public void onCancel (PlatformEnum platform, int action) {
				Log.d (TAG, "onCancel: platform = " + platform + ",action = " + action);
			}
		};
	}

	private IShareHandler.ShareListener obtainShareListener () {
		return new IShareHandler.ShareListener () {
			@Override
			public void onResult (PlatformEnum shareMedia) {
				Log.d (TAG, "onResult: shareMedia = " + shareMedia);
			}

			@Override
			public void onError (PlatformEnum shareMedia, Throwable throwable) {
				Log.d (TAG, "onError: shareMedia = " + shareMedia + ", throwable = " + throwable);
			}

			@Override
			public void onCancel (PlatformEnum shareMedia) {
				Log.d (TAG, "onCancel: shareMedia = " + shareMedia);
			}
		};
	}

	public void deleteAuthorize (Activity activity, PlatformEnum media, ISSOHandler.AuthListener listener) {
		if (this.helper.check (activity, media)) {
			if (listener == null) {
				listener = obtainListener ();
			}
			SSOHandler handler = obtainHandler (media);
			handler.deleteAuthorize (activity, listener);
		}
	}

	public void getPlatformInfo (Activity activity, PlatformEnum media, ISSOHandler.AuthListener listener) {
		if (this.helper.check (activity, media)) {
			if (listener == null) {
				listener = obtainListener ();
			}
			SSOHandler handler = obtainHandler (media);
			handler.callApiProfile (activity, listener);
		}
	}

	public boolean isInstalled (Activity activity, PlatformEnum media) {
		if (this.helper.check (activity, media)) {
			SSOHandler handler = obtainHandler (media);
			handler.isInstalled (activity);
		}
		return false;
	}

	public boolean isAuthorized (Activity activity, PlatformEnum media) {
		if (this.helper.check (activity, media)) {
			SSOHandler handler = obtainHandler (media);
			handler.isAuthorized (activity);
		}
		return false;
	}

	public void applyAuthorize (Activity activity, PlatformEnum media, ISSOHandler.AuthListener listener) {
		if (this.helper.check (activity, media)) {
			if (listener == null) {
				listener = obtainListener ();
			}
			SSOHandler handler = obtainHandler (media);
			handler.applyAuthorize (activity, listener);
		}
	}

	public void share (Activity activity, ShareAction action, IShareHandler.ShareListener listener) {
		if (this.helper.check (activity, action)) {
			if (listener == null) {
				listener = obtainShareListener ();
			}
			PlatformEnum platform = action.getSharePlatform ();
			SSOHandler handler = obtainHandler (platform);
			handler.setCallFrom (action.getShareForm ());
			handler.share (activity, action.getShareContent (), listener);
		}
	}

	static class InnerHelper {
		private Map<PlatformEnum, SSOHandler> map;

		public InnerHelper (Map<PlatformEnum, SSOHandler> map) {
			this.map = map;
		}

		public boolean check (Context context, PlatformEnum media) {
			if (!this.check (context)) {
				return false;
			} else if (!this.check (media)) {
				return false;
			} else {
				SSOHandler handler = this.map.get (media);
				if (!handler.isSupportAuthorize ()) {
					Log.w (TAG, media.toString () + "平台不支持授权,无法完成操作");
					return false;
				} else {
					return true;
				}
			}
		}

		public boolean check (Activity activity, ShareAction action) {
			if (!this.check (activity)) {
				return false;
			} else {
				PlatformEnum shareMedia = action.getSharePlatform ();
				return shareMedia == null ? false : this.check (shareMedia);
			}
		}

		private boolean check (Context context) {
			if (context == null) {
				Log.e (TAG, "Context is null");
				return false;
			} else {
				return true;
			}
		}

		private boolean check (PlatformEnum shareMedia) {
			IPlatform platform = PlatformConfiguration.configs.get (shareMedia);
			if (platform != null && !platform.isConfigured ()) {
				Log.e (TAG, shareMedia + ": 没有配置相关的Appkey、Secret");
				return false;
			} else {
				SSOHandler var3 = this.map.get (shareMedia);
				if (var3 == null) {
					Log.e (TAG, "没有配置 " + shareMedia + " 的jar包");
					return false;
				} else {
					return true;
				}
			}
		}
	}
}
