package com.bookbuf.social.handlers.share.content.location;

import java.io.Serializable;

/**
 * Created by robert on 16/6/29.
 */
public class Location implements Serializable {

	private double latitude;
	private double longitude;

	public Location (double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public double getLatitude () {
		return this.latitude;
	}

	public void setLatitude (double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude () {
		return this.longitude;
	}

	public void setLongitude (double longitude) {
		this.longitude = longitude;
	}

	public String toString () {
		return "(" + this.longitude + "," + this.latitude + ")";
	}

	public static Location build (String s) {
		try {
			String var1 = s.substring (1, s.length () - 1);
			String[] var2 = var1.split (",");
			return new Location (Double.parseDouble (var2[1]), Double.parseDouble (var2[0]));
		} catch (Exception var3) {
			return null;
		}
	}

	public static Location build (android.location.Location location) {
		try {
			if (location.getLatitude () != 0.0D && location.getLongitude () != 0.0D) {
				return new Location (location.getLatitude (), location.getLongitude ());
			}
		} catch (Exception var2) {
			var2.printStackTrace ();
		}

		return null;
	}
}
