package com.bookbuf.social.handlers;

import android.app.Activity;
import android.content.Context;

import com.bookbuf.social.PlatformEnum;
import com.bookbuf.social.handlers.share.content.ShareContent;

import java.util.Map;

/**
 * Created by robert on 16/6/29.
 */
public interface ISSOHandler {


	boolean isInstalled (Context context);

	boolean isSupportAuthorize ();

	boolean isAuthorized (Context context);

	void applyAuthorize (Activity activity, AuthListener listener);

	void deleteAuthorize (Context context, AuthListener listener);

	void callApiProfile (Activity activity, AuthListener listener);

	void callApiCheckAccessTokenValid (Activity activity, AuthListener listener);

	boolean share (Activity activity, ShareContent shareContent, IShareHandler.ShareListener shareListener);

	interface AuthListener {
		int ACTION_AUTHORIZE = 0;
		int ACTION_DELETE = 1;
		int ACTION_GET_PROFILE = 2;

		void onComplete (PlatformEnum platform, int action, Map<String, String> map);

		void onError (PlatformEnum platform, int action, Throwable throwable);

		void onCancel (PlatformEnum platform, int action);
	}

}
